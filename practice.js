if (Meteor.isClient) {

var numberString = "0";

var numbObjOne = "0";
var numbObjTwo = "0";
var result = "0";

var flag = 0;
var operationFlag = 0;

  Template.profilepic.events({
  	'click input.uploadpic' : function () {
  		
  	}
  });
  
  Template.calculator.helpers ({
  	append_function: function (number) {  	
    	numberString = numberString + number;
    	document.getElementById("result").value = Number(numberString);
    }
  });
  
  Template.calculator.events ({
    
  	'click input.zero' : function () {
  		//Template.calculator.helpers.append_function('0');
  		
  		numberString = numberString + 0;
		document.getElementById("result").value = Number(numberString);
  	},
  	
  	'click input.one' : function () {
  		//Meteor.call("append_function", 1);
  		numberString = numberString + 1;
    	document.getElementById("result").value = Number(numberString);
  	},
  	
  	'click input.two' : function () {
  		//Meteor.call("append_function", 2);
  		numberString = numberString + 2;
    	document.getElementById("result").value = Number(numberString);
  	},
  	
  	'click input.three' : function () {
  		//append_function('3');
  		numberString = numberString + 3;
    	document.getElementById("result").value = Number(numberString);
  	},
  	
  	'click input.four' : function () {
  		//append_function('4');
  		numberString = numberString + 4;
    	document.getElementById("result").value = Number(numberString);
  	},
  	
  	'click input.five' : function () {
  		//append_function('5');
  		numberString = numberString + 5;
    	document.getElementById("result").value = Number(numberString);
  	},
  	
  	'click input.six' : function () {
  		//append_function('6');
  		numberString = numberString + 6;
    	document.getElementById("result").value = Number(numberString);
  	},
  	
  	'click input.seven' : function () {
  		//append_function('7');
  		numberString = numberString + 7;
    	document.getElementById("result").value = Number(numberString);
  	},
  	
  	'click input.eight' : function () {
  		//append_function('8');
  		numberString = numberString + 8;
    	document.getElementById("result").value = Number(numberString);
  	},
  	
  	'click input.nine' : function () {
  		//append_function('9');
  		numberString = numberString + 9;
    	document.getElementById("result").value = Number(numberString);
  	},
  	
  	'click input.decimal' : function () {
  		//append_function('.');
  		numberString = numberString + '.';
    	document.getElementById("result").value = numberString;
  	},
  	
  	// for operations
  	
  	'click input.add' : function () {
  	
  		operationFlag = 0;
  	
  		if (flag === 0) {
  			numObjOne = numberString;
  			numberString = "0";
  			flag = 1;
  		}
  	},
  	
  	'click input.subtract' : function () {
  	
  		operationFlag = 1;
  	
  		if (flag === 0) {
  			numObjOne = numberString;
  			numberString = "0";
  			flag = 1;
  		}
  	},

  	'click input.multiply' : function () {
  		//append_function('.');
  		
  		operationFlag = 2;
  		
  		if (flag === 0) {
  			numObjOne = numberString;
  			numberString = "0";
  			flag = 1;
  		}
  	},
  	
  	'click input.divide' : function () {
  		
  		operationFlag = 3;
  	
  		if (flag === 0) {
  			numObjOne = numberString;
  			numberString = "0";
  			flag = 1;
  		}
  	},
  	
  	'click input.sq' : function () {
  		
  		operationFlag = 4;
  	
  		if (flag === 0) {
  			numObjOne = numberString;
  			numberString = "0";
  			flag = 1;
  		}
  	},
  	
  	'click input.sqrt' : function () {
  		
  		operationFlag = 5;
  	
  		if (flag === 0) {
  			numObjOne = numberString;
  			numberString = "0";
  			flag = 1;
  		}
  	},
  	
  	'click input.equals' : function () {
  		numObjTwo = numberString;
  		
  		switch(operationFlag)
  		{
  			case 0:
  				result = parseFloat(numObjOne) + parseFloat(numObjTwo);
  				document.getElementById("result").value = Number(result);
  				break;
  			case 1:
  				result = parseFloat(numObjOne) - parseFloat(numObjTwo);
  				document.getElementById("result").value = Number(result);
  				break;
  			case 2:
  				result = parseFloat(numObjOne) * parseFloat(numObjTwo);
  				document.getElementById("result").value = Number(result);
  				break;
  			case 3:
  				if (numObjOne === 0) {
  					result = 0;
  				} else if (numObjTwo === 0) {
  					result = "unable to divide";
  				} else {
  					result = parseFloat(numObjOne) / parseFloat(numObjTwo);
  				}
  				document.getElementById("result").value = Number(result);
  				break;
  			case 4:
  				result = parseFloat(numObjOne) + parseFloat(numObjTwo);
  				document.getElementById("result").value = Number(Math.sqrt(result));
  				break;
  			case 5:
  				result = parseFloat(numObjOne) + parseFloat(numObjTwo);
  				document.getElementById("result").value = Number(Math.pow(result,2));
  				break;
  			default:
  				result = 0;
  				document.getElementById("result").value = Number(result);
  				
  		}
  	}, 
  	
  	'click input.clear' : function () {
  		numberString = "0";

		numbObjOne = "0";
		numbObjTwo = "0";
		result = 0;

		flag = 0;
		operationFlag = 0;
		
		document.getElementById("result").value = Number(result);
  	}, 
  	
  });
  	
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}
